
Welcome to  the Duelpy documentation!
=====================================
Open-Source Python package for preference-based multi-armed Bandits algorithms.

.. toctree::
   :maxdepth: 2
   :caption: Contents:
   
   duelpy

Indices and tables
==================

* :ref:`genindex`
* :ref:`search`

Fork this project
==================

* https://gitlab.com/duelpy/duelpy